package com.example.recyclerviewpractice.data

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

object RetrofitProvider {

    private val retroObject = Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())

    fun getRickService(): RickService {
        return retroObject
            .baseUrl(RickService.BASE_URL)
            .build()
            .create()
    }

}