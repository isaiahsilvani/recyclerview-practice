package com.example.recyclerviewpractice.data.models

data class Characters(
    val info: Info? = null,
    val results: List<Result>? = null
)