package com.example.recyclerviewpractice.data

object Repository {

    private val rickService = RetrofitProvider.getRickService()

    suspend fun getCharacters() = rickService.getCharacters()

}