package com.example.recyclerviewpractice.data

import com.example.recyclerviewpractice.data.models.Characters
import com.example.recyclerviewpractice.data.models.Result
import retrofit2.http.GET
import retrofit2.http.Path

interface RickService {

    companion object {
        const val BASE_URL = "https://rickandmortyapi.com/api/character/"
        private const val CHARACTER_URL = "{id}"
    }

    @GET(BASE_URL)
    suspend fun getCharacters(): Characters

    @GET(CHARACTER_URL)
    suspend fun getCharacter(@Path("id") id: Int): Result

}