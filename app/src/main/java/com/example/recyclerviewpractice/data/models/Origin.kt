package com.example.recyclerviewpractice.data.models

data class Origin(
    val name: String,
    val url: String
)