package com.example.recyclerviewpractice.view

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.recyclerviewpractice.data.Repository
import com.example.recyclerviewpractice.data.models.Characters
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    private val _data = MutableStateFlow(Characters())
    val data = _data.asStateFlow()

    init {
        viewModelScope.launch {
            _data.value = Repository.getCharacters()
        }
    }

}